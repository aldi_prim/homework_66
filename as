[33mcommit efa030f3b6c9326f3387668b7012d752a960646b[m
Author: Aldiyar Primbayev <primbayev.96aldi@gmail.com>
Date:   Sat Dec 8 17:10:24 2018 +0600

    Run a command that instructs the devise to create a migration to create its own additional fields for the selected model. In our case, this is the User model, accordingly we run such a command: rails generate devise User

[33mcommit 191d2ef491db494eb293ec41c77d3d500020f57a[m
Author: Aldiyar Primbayev <primbayev.96aldi@gmail.com>
Date:   Sat Dec 8 17:05:24 2018 +0600

    model User created

[33mcommit b87c3866ad8f53b4b8f2ebd630da5422b9a313c7[m
Author: Aldiyar Primbayev <primbayev.96aldi@gmail.com>
Date:   Sat Dec 8 17:01:17 2018 +0600

    gem devise initialized and after it advised lines of code added

[33mcommit dbc41c1ae7fbff594d4c0d808ed6563e28471621[m
Author: Aldiyar Primbayev <primbayev.96aldi@gmail.com>
Date:   Sat Dec 8 16:57:17 2018 +0600

    Added gems which will be used in the future

[33mcommit 1a2f698d932818960179b94ee6a8a2c495ac1a22[m
Author: Aldiyar Primbayev <primbayev.96aldi@gmail.com>
Date:   Sat Dec 8 16:53:09 2018 +0600

    Initial commit
