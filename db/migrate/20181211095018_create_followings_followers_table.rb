class CreateFollowingsFollowersTable < ActiveRecord::Migration[5.2]
  def change
    create_table :followings_followers_tables do |t|
      t.references :follower, index: true
      t.references :following, index: true
    end
  end
end
