# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

def copy_avatar_fixture(user)
  file = rand(1..7)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'avatar', "#{file}.jpg")

  user.avatar.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

def copy_image_fixture(post)
  file = rand(1..7)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', 'images', "#{file}.jpg")

  post.images.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

3.times do
  @user = User.create(
    nickname: (nickname = Faker::Name.last_name),
    email: Faker::Internet.unique.email("#{nickname}"),
    password: '123456789',
    password_confirmation: '123456789'
  )
  copy_avatar_fixture(@user)

  5.times do
    @post = Post.create(
      title: Faker::Lorem.sentence(3),
      text: Faker::Lorem.paragraph,
      user_id: @user.id
    )
    copy_image_fixture(@post)
  end
end
