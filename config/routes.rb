Rails.application.routes.draw do

  devise_for :users

  root 'posts#index'

  resources :users, only: [:show]
  post 'subscribe/:follower_id/:following_id' => 'users#subscribe', as: 'subscribe'
  delete 'unsubscribe/:follower_id/:following_id' => 'users#unsubscribe', as: 'unsubscribe'

  resources :posts do
    resources :comments
  end

  resources :likes, only: [:create, :destroy]

  delete 'posts/image/:id/purge_image' => 'posts#purge_image', as: 'purge_image'

end
