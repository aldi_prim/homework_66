class User < ApplicationRecord
  self.table_name = 'users'

  has_one_attached :avatar

  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy

  has_and_belongs_to_many :followings,
    class_name: 'User',
    foreign_key: :follower_id,
    association_foreign_key: :following_id,
    join_table: :followings_followers_tables

  has_and_belongs_to_many :followers,
    class_name: 'User',
    foreign_key: :following_id,
    association_foreign_key: :follower_id,
    join_table: :followings_followers_tables

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :nickname, presence: true, length: { maximum: 50 }
end
