class Post < ApplicationRecord
  has_many_attached :images, dependent: :destroy

  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy

  validates :title, presence: true, length: { maximum: 50 }
  validates :text, presence: true, length: { maximum: 250 }

end
