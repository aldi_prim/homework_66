class LikesController < ApplicationController
  before_action :find_post

  def create
    @like = @post.likes.build(user_id: current_user.id)

    @like.save

    redirect_to @post
  end

  def destroy
    @like = Like.find_by(post_id: @post.id, user_id: current_user.id)
    @like.destroy

    redirect_to @post
  end

  private
    def find_post
      @post = Post.find(params[:post_id])
    end
end
