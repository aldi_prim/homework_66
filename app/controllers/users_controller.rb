class UsersController < ApplicationController
  before_action :find_follower_and_following, only: [:subscribe, :unsubscribe]

  def show
    @user = User.find(params[:id])
    @posts = @user.posts.all.order(created_at: :desc)
  end

  def subscribe
    unless @following_user.followers.include?(@follower_user)
      @following_user.followers << @follower_user
    end

    redirect_back(fallback_location: request.referer)
  end

  def unsubscribe
    if @following_user.followers.include?(@follower_user)
      @following_user.followers.destroy(@follower_user)
    end

    redirect_back(fallback_location: request.referer)
  end

  private
    def find_follower_and_following
      @follower_user = User.find(params[:follower_id])
      @following_user = User.find(params[:following_id])
    end
end
