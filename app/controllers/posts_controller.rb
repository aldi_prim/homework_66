class PostsController < ApplicationController
  def index
    @posts = Post.order(created_at: :desc)
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.build(post_params)

    if @post.save
      redirect_to user_path(@post.user.id)
    else
      render 'new'
    end
  end

  def destroy
    @post = current_user.posts.find(params[:id])
    @post.destroy

    redirect_to user_path(@post.user.id)
  end

  def show
    @post = Post.find(params[:id])
  end

  def purge_image
    image = ActiveStorage::Attachment.find(params[:id])
    image.purge

    redirect_back(fallback_location: request.referer)
  end

  private
    def post_params
      params.require(:post).permit(:title, :text, :user_id, images: [])
    end

end
